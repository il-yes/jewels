<?php

namespace FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use OriginBundle\Entity\Entity;

class EntityController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
    	$entities = $this->getDoctrine()->getManager()->getRepository('OriginBundle:Entity')->findAll();


        return $this->render('FrontBundle:Entity:index.html.twig', array( "entities" => $entities ));
    }
}
