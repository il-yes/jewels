<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\UserRepository")
 */
class User extends BaseUser
{


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="family_name", type="string", length=255)
     */
    private $family_name;



    /**
     * @ORM\OneToMany(targetEntity="OriginBundle\Entity\Address", mappedBy="user")
     */
    private $adresses;


    /**
     * @ORM\OneToMany(targetEntity="OriginBundle\Entity\Command", mappedBy="user")
     */
    private $commandes;



    /**
     * Set familyName
     *
     * @param string $familyName
     *
     * @return User
     */
    public function setFamilyName($familyName)
    {
        $this->family_name = $familyName;

        return $this;
    }

    /**
     * Get familyName
     *
     * @return string
     */
    public function getFamilyName()
    {
        return $this->family_name;
    }

    /**
     * Add adress
     *
     * @param \OriginBundle\Entity\Address $adress
     *
     * @return User
     */
    public function addAdress(\OriginBundle\Entity\Address $adress)
    {
        $this->adresses[] = $adress;

        return $this;
    }

    /**
     * Remove adress
     *
     * @param \OriginBundle\Entity\Address $adress
     */
    public function removeAdress(\OriginBundle\Entity\Address $adress)
    {
        $this->adresses->removeElement($adress);
    }

    /**
     * Get adresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * Add commande
     *
     * @param \OriginBundle\Entity\Command $commande
     *
     * @return User
     */
    public function addCommande(\OriginBundle\Entity\Command $commande)
    {
        $this->commandes[] = $commande;

        return $this;
    }

    /**
     * Remove commande
     *
     * @param \OriginBundle\Entity\Command $commande
     */
    public function removeCommande(\OriginBundle\Entity\Command $commande)
    {
        $this->commandes->removeElement($commande);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandes()
    {
        return $this->commandes;
    }
}
