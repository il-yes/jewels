<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Traduction
 *
 * @ORM\Table(name="traduction")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\TraductionRepository")
 */
class Traduction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="langue", type="string", length=255)
     */
    private $langue;


    /**
     * @ORM\ManyToOne(targetEntity="OriginBundle\Entity\Entity", inversedBy="traductions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $entity;


    /**
    * @ORM\ManyToOne(targetEntity="OriginBundle\Entity\Category", inversedBy="entities", cascade={"persist"})
    * @ORM\JoinColumn(nullable=false)
    */
    private $category;


    /**
     * @ORM\ManyToMany(targetEntity="OriginBundle\Entity\Type", cascade={"persist"})
     */
    private $type;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->type = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Traduction
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Traduction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set langue
     *
     * @param string $langue
     *
     * @return Traduction
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * Get langue
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set entity
     *
     * @param \OriginBundle\Entity\Entity $entity
     *
     * @return Traduction
     */
    public function setEntity(\OriginBundle\Entity\Entity $entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \OriginBundle\Entity\Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set category
     *
     * @param \OriginBundle\Entity\Category $category
     *
     * @return Traduction
     */
    public function setCategory(\OriginBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \OriginBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add type
     *
     * @param \OriginBundle\Entity\Type $type
     *
     * @return Traduction
     */
    public function addType(\OriginBundle\Entity\Type $type)
    {
        $this->type[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \OriginBundle\Entity\Type $type
     */
    public function removeType(\OriginBundle\Entity\Type $type)
    {
        $this->type->removeElement($type);
    }

    /**
     * Get type
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getType()
    {
        return $this->type;
    }
}
