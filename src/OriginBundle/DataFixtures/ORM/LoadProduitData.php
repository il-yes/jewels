<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadCategory.php

namespace OriginBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OriginBundle\Entity\Category;
use OriginBundle\Entity\User;
use OriginBundle\Entity\Type;
use OriginBundle\Entity\Entity;
use OriginBundle\Entity\Traduction;

class LoadProduitData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
      /*
      // Catégories
      $cat = ["Bague","Collier","Bracelet","Montre"];
      $langue = ["fr", "en"];

      for($i=0; $i < count($cat); $i++)
      {
          $category = new Category();
          $category->setNom($cat[$i]);
          $category->setLangue($langue[rand(0,1)]);

          
      }



      // User
      $username = ["Michelle","Erik","Chris","Robert","Paul","Pierre","Jean","Peter"];
      $email = ["a","b","c","d","e","f"];
      $password = ["123456"];

      for($i =0; $i < count($username); $i++)
      {
    
        $user = (new User);

        $user->setUsername($i);
        $user->setFamilyName("Boursin");
        $user->setEmail($email[rand(0,5)].'@mail.com');
        $user->setPassword($password);
      }



      // Traduction
      $nom = ["Rocky","Ferrari","Lamborghini","Web","Développement", "infos", "Mac book Pro", "Métro", "Etats-Unis", "Japon", "Asie", "France", "javascript", "Jquery", "Angular Js"];
      $description = [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", 
        "Morbi lacinia venenatis lorem non commodo. Maecenas ut metus purus.",
        "Suspendisse vel erat non dolor aliquet condimentum. Proin mollis sapien urna, vel dapibus diam vestibulum ut. ",
        "Aenean vitae odio a nisi euismod commodo et sit amet nunc. Etiam tortor eros, maximus sed est a, egestas condimentum purus. Vivamus nec lorem nunc. Phasellus iaculis luctus magna, sed pulvinar lacus. Fusce scelerisque est quis nibh pellentesque, ut dictum dolor suscipit.",
        "Sed hendrerit sit amet nisl mollis pulvinar. ",
        "Nunc consectetur eros et libero auctor condimentum. Donec lacus lectus, condimentum vitae eros at, vulputate lobortis augue. Duis at sapien ut ex egestas pretium sed sed lorem. Duis bibendum dapibus metus non dignissim. Nulla facilisi.", 
        "Quisque congue eu enim et fermentum. ",
        "Pellentesque rhoncus enim id facilisis varius. Sed ac tortor in ipsum sodales posuere ut eu metus. ",
        "Vestibulum viverra odio justo, in iaculis massa rhoncus a. Aliquam purus dolor, malesuada id mattis eu, rhoncus ut massa. ",
        "Fusce scelerisque enim magna, et consectetur est auctor eu. Morbi in consectetur sapien, nec aliquam nisl. ",
        "Integer volutpat fermentum accumsan. Maecenas sit amet ultricies odio. ",
        "Sed quis erat at metus scelerisque hendrerit.",
        "Sed quis molestie orci. Donec vitae neque nec nulla convallis suscipit. ",
        "Fusce imperdiet, magna eget elementum tempor, diam augue mattis dui, sed ullamcorper leo orci eget quam. Curabitur interdum massa in massa varius, quis sollicitudin leo malesuada. Quisque pellentesque nunc ac sem tincidunt pellentesque. Vivamus pulvinar, ex vel vulputate convallis, erat magna porta arcu, a posuere elit tortor a metus. Nunc iaculis fermentum justo, et tristique nunc fringilla vel.",
        "Nunc ullamcorper volutpat volutpat. Duis blandit, leo quis ultricies vestibulum, purus risus efficitur ante, in molestie arcu neque vitae urna. Etiam ornare auctor pulvinar. Ut non condimentum urna, vitae blandit felis.", 
        "Praesent commodo pulvinar orci vestibulum commodo. In eu vehicula lacus, tempus efficitur risus. Integer suscipit tortor vel gravida dictum. Quisque eget erat non est consequat gravida. Integer et eros congue, convallis ante vestibulum, auctor mi. Duis imperdiet lacus id mattis viverra. Praesent tortor erat, aliquam quis venenatis at, suscipit vel odio. Vestibulum eleifend dolor eu convallis congue. Nunc ac diam auctor, semper lorem eget, cursus justo. Proin ultrices mi eu lorem eleifend, ut ullamcorper orci ullamcorper."
      ];

      $trad = (new Traduction);

      $trad->setNom($nom[rand(0,14)]);
      $trad->setLangue($langue[rand(0,1)]);
      $trad->setDescription($description[rand(0,15)]);
      //$trad->setCategory("Bague");
      //$trad->setEntity(rand(0,9));

      

      // Type
      $type = array(
            'Or',
            'Argent',
            'Diamant',
            'Emeraude'
          );

      for($i=0; $i < count($type); $i++)
      {


          $type = new Type();
          $type->setNom($i);

      }*/

          $category = new Category();
          $category->setNom("bob");
          $category->setLangue("fr");

          $type = new Type();
          $type->setNom("bob");



      // Produits
      for($i=0; $i < 10; $i++)
        {
          $entity = new Entity();

          $entity->setPrix(rand(0,100));
          $entity->setStock(rand(0,50));

                  $trad = new Traduction();
           $trad->setNom("bob");
      $trad->setLangue("fr");
      $trad->setDescription("Lorem");
        $trad ->setCategory($category);
        $trad ->addType($type);
          $trad->setEntity($entity);
           $entity->addTraduction($trad);
         

        // On la persiste
        $manager->persist($entity);
        }

      // On déclenche l'enregistrement de toutes les catégories
    
      $manager->flush();
  }
}